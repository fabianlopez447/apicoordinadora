# Método para la conexión de la API de Coordinadora

### Configuración
Antes de poder obtener los datos de la api es necesario configurar ciertos datos que son importantes para solicitar una respuesta valida, de lo contrario retornará un error.

**Intancia**
Debido a que el método está hecho con POO debemos instanciar la clase Coordinadora puesto que es la que tiene todas las funciones rederente al objetivo, en este caso lo haremos con una variable que llamaremos `$coord`:

	$coord = new Coordinadora;

**Datos de usuario**
Es necesario proporcionar los datos para autentificar la solicitud para ellos introducimos la información con el siguiente método:

	$coord->loginInfo_set('usuario', 'contraseña');

**Api Key**
La api key es la clave que los proveedores del servicio ofrecen a los usuarios que vayan a utilizar la infomación que se ofrece, sin está key no se podrá verificar que usted está autorizado para acceder al servicio, para configurarla se utiliza con el siguiente método:

    $coord->apiKey_set('api-key');

**Clave**
Al igual que la api key, la clave es importante para verificar que usted fue autorizado para utilizar la información que se ofrece en el servicio, para configurarla utilizamos el siguiente método: 

    $coord->clave_set('clave-secreta');

### Solicitar información

#### Enviar datos
A la hora de buscar la información con los datos de busquedas se debe realizar una ejecución de dos pasos los cuales serían *asignación* y *ejecución*.

**Asignación**
En la asignación se utiliza un método en el cual se establecen los parametros de búsqueda con los datos que sirvan de filtro para encontrar resultados:

    $coord->find('codigo_remision','nit','div','referencia','image','anexo'); 
	// Los valores pueden ir vacios, al menos uno debe tener un dato

**Ejecución**
Ahora con los datos de busqueda asignado es hora de ejecutarlo. Con el siguiente código:

    $coord->execute();

Bien, así podemos ejecutar el código, ahora la lista de datos que podemos obtener

**Datos del método**

1. **getData()**: Retorna la respuesta de la solicitud al servicio.
1. **getLoginInfo()**: Retorna la clave y usuario configurado.
1. **getApiKey()**: Retorna la apikey configurada.
1. **getClave()**: Retorna la clave configurada.

**Datos de la respuesta**
Una vez se ejecuta el codigo de forma satisfactoria sin errores en la configuración se podrá tener acceso a los siguientes datos.

1. codigo_remision
1. estado
1. novedad
1. tiene_anexo
1. imagen
1. anexos
1. referencia
1. nombre_origen
1. nombre_destino
1. producto
1. guias_vinculadas
1. dias_promesa_servicio

Para más información consultar la documentación del servicio: [Click aqui](http://sandbox.coordinadora.com/ags/1.5/server.php?doc#Seguimiento_simpleOut "Click aqui")