<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <meta charset="UTF-8">
    <title>API coordinadora</title>
</head>
<body>
    <div class="container-fluid">  
        <br>     
        <h2 class="text-center">Seguimiento de pequetes de paquetes Coordinadora</h2>
        <br>   
        <div class="row contenido"> 
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Codigo de remision</th>
                        <th>Estado</th>
                        <th>Novedad</th>
                        <th>Referencia</th>
                        <th>Nombre de origen</th>
                        <th>Nombre de destino</th>
                        <th>Producto</th>
                        <th>Guias vinculadas</th>
                        <th>Dias promesa servicio</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> Dato 1</td>
                        <td> Dato 2</td>
                        <td> Dato 3</td>
                        <td> Dato 4</td>
                        <td> Dato 5</td>
                        <td> Dato 6</td>
                        <td> Dato 7</td>
                        <td> Dato 8</td>
                        <td> Dato 9</td>
                    </tr>
                </tbody>
            </table>
        </div> 
        <div class="text-center">
            <br>
            <a class="btn btn-success btn-lg" href="index.php">Volver</a>
        </div>
        
    
    </div>
    
    
    <script src="js/bootstrap.min.js"></script>
</body>
</html>