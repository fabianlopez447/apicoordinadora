<?PHP
$wsdl = "http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php?wsdl";
$client = new SoapClient($wsdl, array(
    "trace"=>1,
    "exceptions"=>1));

$usuario = "quenty.ws";
$clave  = "65882040cc359cd19eda65f1f1888e943ad50b793e0ce5dd71ff014fb2134f27";

$codigo_remision = $_POST['codigo_remision'];

$parameters= array("codigo_remision"=>$codigo_remision,"usuario"=>$usuario,"clave"=>$clave);

try{
    $values = $client->Guias_liquidacionGuia($parameters);

    $arreglo = '<h3>Guias Liquidación Guia</h3><br><table class="table table-striped table-hover"><thead><tr><th>flete_fijo</th><th>flete_variable</th><thflete_total</th></tr></thead><tbody>';
    foreach ($values as $cont=>$value){
        $arreglo .= '<tr><td>'.$value->flete_fijo.'</td><td>'.$value->flete_variable.'</td><td>'.$value->flete_total.'</td></tr>';
    }
    $arreglo .= '</tbody></table>';
    echo $arreglo;

}catch(SoapFault $fault){
    echo '<div class="alert alert-danger" role="alert"><b>Mensaje de Error: </b>'.$fault->getMessage().'</div>';
}
