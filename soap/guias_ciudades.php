<HTML>
    <HEAD>
        <TITLE>Título de la página</TITLE>
        <link rel="stylesheet" href="lib/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    </HEAD>

    <BODY>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>4.- Guias.ciudades</h1>
                <hr>
            </div>
            <div class="col-sm-6 text-right">
                <a id="volver" name="volver" class="btn btn-danger" href="javascript: window.history.back();">Volver</a>
            </div>
            <div class="col-sm-6 text-left">
                <button id="generar" name="generar" class="btn btn-primary" type="button">Generar</button>
            </div>
            <div class="col-sm-12 text-center">
                <hr>
                <div id="DivResultado"></div>
            </div>

        </div>
    </div>

    <script src="lib/jquery.min.js"></script>



<script>
    $(document).ready(function(){

        $("#generar").click(function(){
            $("#DivResultado").html('<div align="center">Estableciendo Comunicación con el Web Service y Generando Respuesta... Por favor espere...<br><img src="img/loader.gif"/></div>');
            $.ajax({
                type: 'POST',
                url: 'guias_ciudades2.php',
                //dataType: "json",
                //data: {},
                success: function(datos){
                    $('#DivResultado').html(datos);
                }
            });

        });
    });
</script>

    </BODY>
</HTML>