<HTML>
    <HEAD>
        <TITLE>Título de la página</TITLE>
        <link rel="stylesheet" href="lib/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    </HEAD>

    <BODY>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>4.- Guias.generarDespacho</h1>
                <hr>
            </div>
            <div class="col-sm-12 text-center">
                <a id="volver" name="volver" class="btn btn-danger" href="javascript: window.history.back();">Volver</a>
                <hr>
            </div>

            <div class="col-sm-6 text-center">
                <form>
                    <div class="form-group text-center">
                        <label for="codigo_remision">Tipo de Impresion:</label>
                        <select class="form-control" id="tipo_impresion" name="tipo_impresion">
                            <option value="">Seleccione</option>
                            <option value="LASER">LASER</option>
                            <option value="LASER_RESUMIDA">LASER RESUMIDA</option>
                            <option value="POS_PDF">POS PDF</option>
                            <option value="POS_PLANO">POS_PLANO</option>
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <label for="codigo_remision">Margen Izquierdo:</label>
                        <input type="text" id="margen_izquierdo" name="margen_izquierdo" class="form-control" placeholder="Margen izquierdo para la generación del PDF, en caso que el tipo impresion seleccionado genere un PDF">
                    </div>
                    <div class="form-group text-center">
                        <label for="codigo_remision">Margen Superior:</label>
                        <input type="text" id="margen_superior" name="margen_superior" class="form-control" placeholder="Margen superior para la generación del PDF, en caso que el tipo impresion seleccionado genere un PDF">
                    </div>
                    <div class="form-group text-center">
                        <label for="codigo_remision">Agregar Codigo de Remisión al cojunto de Guias:</label>
                        <div class="input-group">
                            <input type="text" id="codigo_remision" name="codigo_remision" class="form-control" placeholder="Códigos de guia a las que se les va a generar un despacho">
                            <div class="input-group-btn">
                                <button id="btnAgregar" name="btnAgregar" class="btn btn-success" type="button">Agregar</button>
                            </div>
                        </div>
                    </div>

                </form>
                <hr>
            </div>
            <div class="col-sm-6 text-center">
                <label for="codigo_remision">Cojunto de Guias:</label>
                <div id="DivConjunto"></div>
                <hr>
                <button id="btnEnviar" name="btnEnviar" class="btn btn-primary" type="button">Generar Despacho</button>
                <input type="hidden" id="codigos" name="codigos" value="">
            </div>
            <div class="col-sm-12 text-center">
                <div id="DivResultado"></div>
            </div>

        </div>
    </div>

    <script src="lib/jquery.min.js"></script>



<script>
    $(document).ready(function(){

        $("#btnAgregar").click(function(){
            var codigo_remision = $('#codigo_remision').val();

            if($('#codigos').val()!=''){
                $('#codigos').val($('#codigos').val()+' | '+codigo_remision);
            }else{
                $('#codigos').val(codigo_remision);
            }
            $('#codigo_remision').val('');

            $("#DivConjunto").html('<div align="center">'+$('#codigos').val()+'</div>');
        });


        $("#btnEnviar").click(function(){

            var margen_izquierdo= $('#margen_izquierdo').val();
            var margen_superior = $('#margen_superior').val();
            var tipo_impresion  = $('#tipo_impresion').val();
            var codigos         = $('#codigos').val();

            $("#DivResultado").html('<div align="center">Estableciendo Comunicación con el Web Service y Generando Respuesta... Por favor espere...<br><img src="img/loader.gif"/></div>');
            $.ajax({
                type: 'POST',
                url: 'Guias.generarDespacho2.php',
                data: {
                    codigos:codigos,
                    margen_izquierdo:margen_izquierdo,
                    margen_superior:margen_superior,
                    tipo_impresion:tipo_impresion
                },
                success: function(datos){

                    $('#margen_izquierdo').val('');
                    $('#margen_superior').val('');
                    $('#tipo_impresion').val('');
                    $('#codigos').val('');
                    $("#DivConjunto").html('');

                    $('#DivResultado').html(datos);
                }
            });
        });

    });
</script>

    </BODY>
</HTML>