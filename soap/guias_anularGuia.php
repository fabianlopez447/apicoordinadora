<HTML>
    <HEAD>
        <TITLE>Título de la página</TITLE>
        <link rel="stylesheet" href="lib/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </HEAD>

    <BODY>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>2.- Guias.anularGuia</h1>
                <hr>
            </div>
            <div class="col-sm-12 text-center">
                <a id="volver" name="volver" class="btn btn-danger" href="javascript: window.history.back();">Volver</a>
                <hr>
            </div>

            <div class="col-sm-12">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="codigo_remision">Codigo Remisión:</label>
                        <input type="text" id="codigo_remision" name="codigo_remision" class="form-control" placeholder="Número de la guia que se desea anular">
                    </div>
                    <button id="btnEnviar" name="btnEnviar" class="btn btn-primary" type="button">Enviar</button>
                </form>
                <hr>
            </div>
            <div class="col-sm-12 text-center">
                <div id="DivResultado"></div>
            </div>

        </div>
    </div>

    <script src="lib/jquery.min.js"></script>



<script>
    $(document).ready(function(){

        $("#btnEnviar").click(function(){
            var codigo_remision = $('#codigo_remision').val();
            $("#DivResultado").html('<div align="center">Estableciendo Comunicación con el Web Service y Generando Respuesta... Por favor espere...<br><img src="img/loader.gif"/></div>');
            $.ajax({
                type: 'POST',
                url: 'guias_anularGuia2.php',
                data: {codigo_remision:codigo_remision},
                success: function(datos){
                    $('#DivResultado').html(datos);
                }
            });

        });
    });
</script>

    </BODY>
</HTML>