<?php
if(isset($_POST['detalles']) && $_POST['detalles']!='') {
    $detalles = explode('|', $_POST['detalles']);

    foreach ($detalles as $pos => $detalle) {
        $valores = explode(',', $detalle);
        $arreglo[$pos] = $valores;
    }
}

$url = "http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php";

$p = new stdClass();

$p->codigo_remision = "";
$p->fecha = "";
$p->id_cliente = 26935;
$p->id_remitente = 0;
$p->nombre_remitente = "Quenty S.A.S.";
$p->direccion_remitente = "CALLE 25C BIS #99-55";
$p->telefono_remitente = "3103237935";
$p->ciudad_remitente = "11001000";

$p->nit_destinatario = $_POST['nit_destinatario'];
$p->div_destinatario = $_POST['div_destinatario'];
$p->nombre_destinatario = $_POST['nombre_destinatario'];
$p->direccion_destinatario = $_POST['direccion_destinatario'];
$p->ciudad_destinatario = $_POST['ciudad_destinatario'];
$p->telefono_destinatario = $_POST['telefono_destinatario'];
$p->valor_declarado = $_POST['valor_declarado'];
$p->codigo_cuenta = $_POST['codigo_cuenta'];
$p->codigo_producto = $_POST['codigo_producto'];
$p->nivel_servicio = $_POST['nivel_servicio'];
$p->linea = $_POST['linea'];
$p->contenido = $_POST['contenido'];
$p->referencia = $_POST['referencia'];
$p->observaciones = $_POST['observaciones'];
$p->estado = $_POST['estado'];

$p->detalle = array();

$total_detalles = $_POST['ContadorD'];

for($i=0; $i<$total_detalles; $i++){

    $item = new stdClass();
    $item->ubl = $arreglo[$i][0];
    $item->alto = $arreglo[$i][1];
    $item->ancho = $arreglo[$i][2];
    $item->largo = $arreglo[$i][3];
    $item->peso = $arreglo[$i][4];
    $item->unidades = $arreglo[$i][5];
    $item->referencia = $arreglo[$i][6];
    $item->nombre_empaque = $arreglo[$i][7];

    array_push($p->detalle, $item);

}

$p->recaudos = array();

$recaudo = new stdClass();
$recaudo->referencia = '8001145341';
$recaudo->valor = '279000';
$recaudo->valor_base_iva = '231897';
$recaudo->valor_iva = '47103';
$recaudo->forma_pago = '1';

array_push($p->recaudos, $recaudo);


$p->cuenta_contable = "";
$p->centro_costos = "";
$p->recaudos = "";
$p->margen_izquierdo = "3";
$p->margen_superior = "1";
$p->id_rotulo = 12;
$p->formato_impresion = 0;
$p->usuario_vmi = "";
$p->atributo1_nombre = "";
$p->atributo1_valor = "";
$p->notificaciones = "";
$p->atributos_retorno = "";
$p->nro_doc_radicados = "";
$p->nro_sobre = "";
$p->usuario = "eflogistic.ws";
$p->clave = "f0be993a-eebc-11e8-8eb2-f2801f1b9fd1";


$client = new SoapClient(null, array("location" => $url,
    "uri" => $url,
    "use" => SOAP_LITERAL,
    "trace" => true,
    "exceptions" => true,
    "soap_version" => SOAP_1_2,
    "connection_timeout" => 30,
    "encoding" => "utf-8"));


try {
    $res = $client->Guias_generarGuia($p);
    $pdf_guia = base64_decode($res->pdf_guia);
    echo "<table class='table table-striped table-hover'> <tr> <th>id_remision</th> <td>".$res->id_remision."</td></tr><tr> <th>codigo_remision</th> <td>".$res->codigo_remision."</td></tr><tr> <th>pdf_guia</th> <td><a href='$res->pdf_guia'>$res->pdf_guia</a></td></tr><tr> <th>pdf_rotulo</th> <td><a href='".$res->pdf_rotulo."' target='_blank'>Ver PDF ROTULO</a></td></tr><tr> <th>url_terceros</th> <td><a href='".$res->url_terceros."' target='_blank'>".$res->url_terceros."</a></td></tr></table>";
    //echo "<table class='table table-striped table-hover'> <tr> <th>id_remision</th> <td>".$res->id_remision."</td></tr><tr> <th>codigo_remision</th> <td>".$res->codigo_remision."</td></tr><tr> <th>pdf_guia</th> <td><a href='#' onclick='javascript: VerGuiaPDF()'>Ver PDF GUIA</a></td></tr><tr> <th>pdf_rotulo</th> <td><a href='#' target='_blank'>Ver PDF ROTULO</a></td></tr><tr> <th>url_terceros</th> <td><a href='".$res->url_terceros."' target='_blank'>".$res->url_terceros."</a></td></tr></table>";



} catch (SoapFault $e) {
    echo "<div class=\"alert alert-danger\" role=\"alert\">" . $e->getMessage() . "</div>";
}

?>

