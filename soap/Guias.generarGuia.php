<?php
if(isset($_POST['btnEnviar'])){

    if(isset($_POST['detalles']) && $_POST['detalles']!='') {
        $detalles = explode('|', $_POST['detalles']);

        foreach ($detalles as $pos => $detalle) {
            $valores = explode(',', $detalle);
            $arreglo[$pos] = $valores;
        }
    }

    $url = "http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php";

    $p = new stdClass();

    $p->codigo_remision = "";
    $p->fecha = "";
    $p->id_cliente = 26935;
    $p->id_remitente = 0;
    $p->nombre_remitente = "Quenty S.A.S.";
    $p->direccion_remitente = "CALLE 25C BIS #99-55";
    $p->telefono_remitente = "3103237935";
    $p->ciudad_remitente = "11001000";

    $p->nit_destinatario = $_POST['nit_destinatario'];
    $p->div_destinatario = $_POST['div_destinatario'];
    $p->nombre_destinatario = $_POST['nombre_destinatario'];
    $p->direccion_destinatario = $_POST['direccion_destinatario'];
    $p->ciudad_destinatario = $_POST['ciudad_destinatario'];
    $p->telefono_destinatario = $_POST['telefono_destinatario'];
    $p->valor_declarado = $_POST['valor_declarado'];
    $p->codigo_cuenta = $_POST['codigo_cuenta'];
    $p->codigo_producto = $_POST['codigo_producto'];
    $p->nivel_servicio = $_POST['nivel_servicio'];
    $p->linea = $_POST['linea'];
    $p->contenido = $_POST['contenido'];
    $p->referencia = $_POST['referencia'];
    $p->observaciones = $_POST['observaciones'];
    $p->estado = $_POST['estado'];

    $p->detalle = array();

    $total_detalles = $_POST['ContadorD'];

    for($i=0; $i<$total_detalles; $i++){

        $item = new stdClass();
        $item->ubl = $arreglo[$i][0];
        $item->alto = $arreglo[$i][1];
        $item->ancho = $arreglo[$i][2];
        $item->largo = $arreglo[$i][3];
        $item->peso = $arreglo[$i][4];
        $item->unidades = $arreglo[$i][5];
        $item->referencia = $arreglo[$i][6];
        $item->nombre_empaque = $arreglo[$i][7];

        array_push($p->detalle, $item);

    }

    $p->recaudos = array();

    $recaudo = new stdClass();
    $recaudo->referencia = '8001145341';
    $recaudo->valor = '279000';
    $recaudo->valor_base_iva = '231897';
    $recaudo->valor_iva = '47103';
    $recaudo->forma_pago = '1';

    array_push($p->recaudos, $recaudo);


    $p->cuenta_contable = "";
    $p->centro_costos = "";
    $p->recaudos = "";
    $p->margen_izquierdo = "3";
    $p->margen_superior = "1";
    $p->id_rotulo = 12;
    $p->formato_impresion = 0;
    $p->usuario_vmi = "";
    $p->atributo1_nombre = "";
    $p->atributo1_valor = "";
    $p->notificaciones = "";
    $p->atributos_retorno = "";
    $p->nro_doc_radicados = "";
    $p->nro_sobre = "";
    $p->usuario = "quenty.ws";
    $p->clave = "65882040cc359cd19eda65f1f1888e943ad50b793e0ce5dd71ff014fb2134f27";


    $client = new SoapClient(null, array("location" => $url,
        "uri" => $url,
        "use" => SOAP_LITERAL,
        "trace" => true,
        "exceptions" => true,
        "soap_version" => SOAP_1_2,
        "connection_timeout" => 30,
        "encoding" => "utf-8"));


    try {
        $res = $client->Guias_generarGuia($p);
        $pdf_guia = $res->pdf_guia;
        $pdf_rotulo = $res->pdf_rotulo;

        $respuesta = "<table class='table table-striped table-hover'> <tr> <th>id_remision</th> <td>".$res->id_remision."</td></tr><tr> <th>codigo_remision</th> <td>".$res->codigo_remision."</td></tr><tr> <th>pdf_guia</th> <td><button type='button' id='btonGuia' name='btonGuia'>Ver PDF GUIA</button></td></tr><tr> <th>pdf_rotulo</th> <td><button type='button' id='btonRotulo' name='btonRotulo'>Ver PDF ROTULO</button></td></tr><tr> <th>url_terceros</th> <td><a href='".$res->url_terceros."' target='_blank'>".$res->url_terceros."</a></td></tr></table>";

    } catch (SoapFault $e) {
        $respuesta = "<div class=\"alert alert-danger\" role=\"alert\">" . $e->getMessage() . "</div>";
    }

}
?>
<HTML>
    <HEAD>
        <TITLE>Título de la página</TITLE>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </HEAD>

    <BODY>
    <?php if(!isset($_POST['btnEnviar'])){ ?>
    <form id="form1" name="form1" method="post" action="" autocomplete="off">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>1.- Guias.generarGuia</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <legend><i class="glyphicon glyphicon-send"></i> Datos de la Nueva Guia</legend>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="nit_destinatario">Nit del destinatario:</label><span class="label label-default">Ej. 891930888</span>
                            <input type="text" id="nit_destinatario" name="nit_destinatario" class="form-control" placeholder="Nit del destinatario">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="div_destinatario">División del destinatario:</label><span class="label label-default">Ej. 01</span>
                            <input type="text" id="div_destinatario" name="div_destinatario" class="form-control" placeholder="División del destinatario">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="nombre_destinatario">Nombre del destinatario:</label><span class="label label-default">Ej. Prueba</span>
                            <input type="text" id="nombre_destinatario" name="nombre_destinatario" class="form-control" placeholder="Nombre del destinatario">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="direccion_destinatario">Dirección del destinatario:</label><span class="label label-default">Ej. Kr24 #45-03</span>
                            <input type="text" id="direccion_destinatario" name="direccion_destinatario" class="form-control" placeholder="Dirección del destinatario">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="ciudad_destinatario">Ciudad del destinatario:</label><span class="label label-default">Ej. 11001000</span>
                            <select class="form-control" id="ciudad_destinatario" name="ciudad_destinatario">
                                <option value="">Seleccione</option>
                                <option value="05001000">MEDELLIN</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="telefono_destinatario">Teléfono del destinatario:</label><span class="label label-default">Ej. 58700000</span>
                            <input type="text" id="telefono_destinatario" name="telefono_destinatario" class="form-control" placeholder="Teléfono del destinatario">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="valor_declarado">Valor declarado del envío:</label><span class="label label-default">Ej. 500000</span>
                            <input type="text" id="valor_declarado" name="valor_declarado" class="form-control" placeholder="Valor declarado del envío">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="codigo_cuenta">Cuenta:</label><span class="label label-default">Ej. Cuenta Corriente</span>
                            <select class="form-control" id="codigo_cuenta" name="codigo_cuenta">
                                <option value="">Seleccione</option>
                                <option value="1">Cuenta Corriente</option>
                                <option value="2">Acuerdo Semanal</option>
                                <option value="3">Flete Pago</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="codigo_producto">Código producto:</label><span class="label label-default">Ej. Auto</span>
                            <select class="form-control" id="codigo_producto" name="codigo_producto">
                                <option value="">Seleccione</option>
                                <option value="0">Auto</option>
                                <option value="1">Mercancia</option>
                                <option value="2">Paquetes de 1-2 Kg</option>
                                <option value="3">Documentos</option>
                                <option value="6">Paquetes de 3-5 Kg</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="nivel_servicio">Nivel de servicio:</label><span class="label label-default">Ej. Estandar</span>
                            <select class="form-control" id="nivel_servicio" name="nivel_servicio">
                                <option value="">Seleccione</option>
                                <option value="1">Estandar</option>
                                <option value="2">Express</option>
                                <option value="3">Programado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="linea">Linea:</label>
                            <input type="text" id="linea" name="linea" class="form-control" placeholder="Linea, si no se maneja se envia vacio">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contenido">Contenido del envío:</label><span class="label label-default">Ej. Zapatos</span>
                            <input type=codigo_producto"text" id="contenido" name="contenido" class="form-control" placeholder="Contenido del envío">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="referencia">Documento referencia:</label>
                            <input type="text" id="referencia" name="referencia" class="form-control" placeholder="Documento con el cual se quiere relacionar el envio para consultas posteriores">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="observaciones">Observaciones:</label><span class="label label-default">Ej. Prueba</span>
                            <input type="text" id="observaciones" name="observaciones" class="form-control" placeholder="Margen izquierdo para la generación del PDF, en caso que el tipo impresion seleccionado genere un PDF">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="estado">Estado:</label><span class="label label-default">Ej. IMPRESO</span>
                            <select class="form-control" id="estado" name="estado">
                                <option value="">Seleccione</option>
                                <option value="PENDIENTE">PENDIENTE</option>
                                <option value="IMPRESO">IMPRESO</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <legend><i class="glyphicon glyphicon-list"></i> Detalle de la guia. Se debe enviar al menos un registro.</legend>
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="ubl">Código de la Ubl:</label><span class="label label-default">Ej. 0</span>
                            <input type="text" id="ubl" name="ubl" class="form-control" placeholder="Código de la Ubl con la que se va a liquidar">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="alto">Alto en centimetros:</label><span class="label label-default">Ej. 30</span>
                            <input type="text" id="alto" name="alto" class="form-control" placeholder="Alto en centimetros">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="ancho">Ancho en centimetros:</label><span class="label label-default">Ej. 50</span>
                            <input type="text" id="ancho" name="ancho" class="form-control" placeholder="Ancho en centimetros">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="largo">Largo en centimetros:</label><span class="label label-default">Ej. 60</span>
                            <input type="text" id="largo" name="largo" class="form-control" placeholder="Largo en centimetros">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="peso">Peso en Kg:</label><span class="label label-default">Ej. 30</span>
                            <input type="text" id="peso" name="peso" class="form-control" placeholder="Peso en Kg">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="unidades">Unidades:<br></label><span class="label label-default">Ej. 2</span><br>&nbsp;
                            <input type="text" id="unidades" name="unidades" class="form-control" placeholder="Unidades">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="referenciad">Referencia de la unidad<br>de empaque:</label>
                            <input type="text" id="referenciad" name="referenciad" class="form-control" placeholder="Referencia de la unidad de empaque.">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="nombre_empaque">Nombre del<br>empaque:</label>
                            <input type="text" id="nombre_empaque" name="nombre_empaque" class="form-control" placeholder="Nombre del empaque.">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <br>
                        <button class="btn btn-success" id="btnAgregarDetalle" name="btnAgregarDetalle" type="button">Agregar<br>Detalle</button>
                        <input type="hidden" name="ContadorD" id="ContadorD" value="0">
                        <input type="hidden" name="detalles" id="detalles" value="">
                        <input type="hidden" name="tabla_cuerpo" id="tabla_cuerpo" value="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="DivTablaDetalleError"></div>
                        <div id="DivTablaDetalle"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr>
                <a id="volver" name="volver" class="btn btn-danger" href="javascript: window.history.back();">Volver</a>
                <button id="btnEnviar" name="btnEnviar" class="btn btn-primary" type="submit" value="1">Generar Guia</button>
            </div>
        </div>
    </div>
    </form>
    <?php }else{ ?>
    <div class="container" id="DivResultado0">
        <div class="row">
            <div class="col-sm-12">
                <h1>1.- Guias.generarGuia</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <a id="volver" name="volver" class="btn btn-danger" href="index.php">Volver</a>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <legend><i class="glyphicon glyphicon-road"></i> Datos de la Nueva Guia Generada</legend>
                <div id="DivResultado">
                    <?php echo $respuesta; ?>
                    <input type="hidden" value="<?php echo $pdf_guia; ?>" id="pdf_guia" name="pdf_guia">
                    <input type="hidden" value="<?php echo $pdf_rotulo; ?>" id="pdf_rotulo" name="pdf_rotulo">
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/jquery.base64.min.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
        var tabla_inicio    = '<table class="table table-striped table-hover"> <thead> <tr> <th>ubl</th> <th>alto</th> <th>ancho</th> <th>largo</th> <th>peso</th> <th>unidades</th> <th>referencia</th> <th>nombre_empaque</th> </tr></thead> <tbody>';
        var tabla_fin       = ' </tbody></table>';


        $("#btnAgregarDetalle").click(function(){

            $("#DivTablaDetalleError").html('');
            var arreglo = [];
            var tabla_cuerpo    = $('#tabla_cuerpo').val();
            var ubl             = $('#ubl').val();
            var alto            = $('#alto').val();
            var ancho           = $('#ancho').val();
            var largo           = $('#largo').val();
            var peso            = $('#peso').val();
            var unidades        = $('#unidades').val();
            var referencia      = $('#referenciad').val();
            var nombre_empaque  = $('#nombre_empaque').val();

            if(ubl!='' && alto!='' && ancho!='' && largo!='' && peso!='' && unidades!=''){

                if($('#detalles').val()!=''){
                    arreglo = "|"+ubl+","+alto+","+ancho+","+largo+","+peso+","+unidades+",'"+referencia+"','"+nombre_empaque+"'";
                }else{
                    arreglo = ubl+","+alto+","+ancho+","+largo+","+peso+","+unidades+",'"+referencia+"','"+nombre_empaque+"'";
                }

                $('#detalles').val($('#detalles').val()+arreglo);
                $('#ContadorD').val(parseInt($('#ContadorD').val())+1);

                tabla_cuerpo = tabla_cuerpo+'<tr> <td>'+ubl+'</td><td>'+alto+'</td><td>'+ancho+'</td><td>'+largo+'</td><td>'+peso+'</td><td>'+unidades+'</td><td>'+referencia+'</td><td>'+nombre_empaque+'</td></tr>';
                $('#tabla_cuerpo').val(tabla_cuerpo);

                $("#DivTablaDetalle").html(tabla_inicio+tabla_cuerpo+tabla_fin);

                $('#ubl').val('');
                $('#alto').val('');
                $('#ancho').val('');
                $('#largo').val('');
                $('#peso').val('');
                $('#unidades').val('');
                $('#referenciad').val('');
                $('#nombre_empaque').val('');
            }else{
                $("#DivTablaDetalleError").html('<div class="alert alert-danger" role="alert">Los campos: UBL, Alto, Ancho, Largo, Peso y Unidades son Obligatorios</div>');
            }

        });

/*
        $("#btnEnviar").click(function(){

            var datos_formulario = $("#form1").serialize();

            $("#DivResultado").html('<div align="center">Estableciendo Comunicación con el Web Service y Generando Respuesta... Por favor espere...<br><img src="img/loader.gif"/></div>');
            $.ajax({
                type: 'POST',
                url: 'Guias.generarGuia2.php',
                data: datos_formulario,
                success: function(datos){
                    //$('#DivFormulario').css('display', 'none');
                    $('#DivResultado0').css('display', 'block');
                    $("#DivResultado").html(datos);
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {

                if (jqXHR.status === 0) {

                    alert('Not connect: Verify Network.');

                } else if (jqXHR.status == 404) {

                    alert('Requested page not found [404]');

                } else if (jqXHR.status == 500) {

                    alert('Internal Server Error [500].');

                } else if (textStatus === 'parsererror') {

                    alert('Requested JSON parse failed.');

                } else if (textStatus === 'timeout') {

                    alert('Time out error.');

                } else if (textStatus === 'abort') {

                    alert('Ajax request aborted.');

                } else {

                    alert('Uncaught Error: ' + jqXHR.responseText);

                }

            });
        });
*/
    });

    $("#btonGuia").click(function(){
        var pdf = $('#pdf_guia').val();
        var pdfText=$.base64.decode(pdf);

        var winlogicalname = "detailPDF";
        var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
            'resizable,screenX=50,screenY=50,width=850,height=1050';

        var htmlText = '<embed width=100% height=100%'
            + ' type="application/pdf"'
            + ' src="data:application/pdf,'
            + escape(pdfText)
            + '"></embed>';

        $("#DivResultadoCreateHAWB").html('');
        // Open PDF in new browser window
        var detailWindow = window.open ("", winlogicalname, winparams);
        detailWindow.document.write (htmlText);
        detailWindow.document.close ();
    });

    $("#btonRotulo").click(function(){
        var pdf = $('#pdf_rotulo').val();
        var pdfText=$.base64.decode(pdf);

        var winlogicalname = "detailPDF";
        var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
            'resizable,screenX=50,screenY=50,width=850,height=1050';

        var htmlText = '<embed width=100% height=100%'
            + ' type="application/pdf"'
            + ' src="data:application/pdf,'
            + escape(pdfText)
            + '"></embed>';

        $("#DivResultadoCreateHAWB").html('');
        // Open PDF in new browser window
        var detailWindow = window.open ("", winlogicalname, winparams);
        detailWindow.document.write (htmlText);
        detailWindow.document.close ();
    });


</script>

    </BODY>
</HTML>
