<?PHP
$wsdl = "http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php?wsdl";
$client = new SoapClient($wsdl, array(
    "trace"=>1,
    "exceptions"=>1));

$usuario = "eflogistic.ws";
$clave  = "65882040cc359cd19eda65f1f1888e943ad50b793e0ce5dd71ff014fb2134f27";

$margen_izquierdo= $_POST['margen_izquierdo'];
$margen_superior = $_POST['margen_superior'];
$tipo_impresion  = $_POST['tipo_impresion'];
$codigos         = $_POST['codigos'];
$guias = explode(" | ",$codigos);

$parameters= array("guias"=>$guias,"margen_izquierdo"=>$margen_izquierdo,"margen_superior"=>$margen_superior,"tipo_impresion"=>$tipo_impresion,"usuario"=>$usuario,"clave"=>$clave);

try{
    $values = $client->Guias_generarDespacho($parameters);

    $arreglo = '<h3>Guias Ciudades</h3><br><table class="table table-striped table-hover"><thead><tr><th>Codigo</th><th>Nombre</th><th>Codigo Departamento</th><th>Nombre Departamento</th></tr></thead><tbody>';
    foreach ($values as $cont=>$value){
        $arreglo .= '<tr><td>'.$value->codigo.'</td><td>'.$value->nombre.'</td><td>'.$value->codigo_departamento.'</td><td>'.$value->nombre_departamento.'</td></tr>';
    }
    $arreglo .= '</tbody></table>';
    echo $arreglo;

}catch(SoapFault $fault){
    echo '<div class="alert alert-danger" role="alert"><b>Mensaje de Error: </b>'.$fault->getMessage().'</div>';
}
