<?PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');

//require_once('nusoap-0.9.5/lib/nusoap.php');

$wsdl = "http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php?wsdl";
$client = new SoapClient($wsdl, array(
    "trace"=>1,
    "exceptions"=>1));

$usuario = "quenty.ws";
$clave  = "65882040cc359cd19eda65f1f1888e943ad50b793e0ce5dd71ff014fb2134f27";

$parameters= array("usuario"=>$usuario,"clave"=>$clave);

try{
    $values = $client->Guias_ciudades($parameters);

    //$arreglo = '[';
    $arreglo = '<h3>Guias Ciudades</h3><br><table class="table table-striped table-hover"><thead><tr><th>Codigo</th><th>Nombre</th><th>Codigo Departamento</th><th>Nombre Departamento</th></tr></thead><tbody>';
    foreach ($values as $cont=>$value){
        $arreglo .= '<tr><td>'.$value->codigo.'</td><td>'.$value->nombre.'</td><td>'.$value->codigo_departamento.'</td><td>'.$value->nombre_departamento.'</td></tr>';
    }
    $arreglo .= '</tbody></table>';
    echo $arreglo;

}catch(SoapFault $fault){
    echo '<div class="alert alert-danger" role="alert"><b>Mensaje de Error: </b>'.$fault->getMessage().'</div>';
}



