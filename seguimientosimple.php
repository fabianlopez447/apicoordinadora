<?php
    class Coordinadora {
        public $loginInfo;
        public $apikey;
        public $clave;
        public $url = "http://sandbox.coordinadora.com/ags/1.5/server.php?wsdl";
        public $p;
        public $values;

        // Métodos get para obtener los valores de las variables
        public function loginInfo_get(){
            return $this->loginInfo;
        }

        public function apiKey_get() {
            return $this->apikey;
        }

        public function clave_get() {
            return $this->clave;
        }

        public function getData(){
            return $this->values;
        }


        // Métodos set para darle valor a las variables
        public function find_set($_remision, $_nit, $_div, $_referencia, $_imagen, $_anexo){
            $p=new stdClass();
            $p->codigo_remision= $_remision;
            $p->nit= $_nit;
            $p->div= $_div;
            $p->referencia= $_referencia;
            $p->imagen= $_imagen;
            $p->anexo=$_anexo;
            $p->apikey=$this->apikey;
            $p->clave=$this->clave;
            $p->LoginInfo = $this->loginInfo;
            $this->p = $p;
        }

        public function apiKey_set($_apikey){
            $this->apikey = $_apikey;
        }

        public function clave_set($_clave){
            $this->clave = $_clave;
        }

        public function loginInfo_set($_userName, $_passWord){
            $this->loginInfo = ['userName'=> $_userName, 'passWord' => $_passWord];
        }

        public function setData($_data){
            $this->values = $_data;
        }

        // Método para obtener los datos solicitados segun los parametros enviados
        // Una funcion de callback solo para tener un nombre bonito
        public function find($_remision, $_nit, $_div, $_referencia, $_imagen, $_anexo){
            $this->find_set($_remision, $_nit, $_div, $_referencia, $_imagen, $_anexo);
        }

        // Ejecuta la funcion principal con el servidor y captura la respuesta segun sea el resultado
        public function execute(){
            try {        
                $options = array( 
                    'soap_version'=>SOAP_1_1, 
                    'exceptions'=>true, 
                    'trace'=>1, 
                    'cache_wsdl'=>WSDL_CACHE_NONE 
                );
                $client = new SoapClient($this->url,$options);
                $response = $client->Seguimiento_simple(['p' => $this->p]);
                echo ($this->setData($response));
            } catch (Exception $e) { 
                echo $e->getMessage(); 
            }
        }
    }


    $coord = new Coordinadora;
    $coord->apiKey_set('f0be993a-eebc-11e8-8eb2-f2801f1b9fd1');
    $coord->clave_set('1q7!7?|0Tk225Zb');
    $coord->loginInfo_set('eflogistic.ws', 'cfb84c9cc651a9d8f9aeb07a54ded051f0760a842ee6a03690420be4203f1f4d');
    $coord->find('77590000576','','','','','');
    $coord->execute();

    if (isset($coord->getData()->Seguimiento_simpleResult)) {
        $datos = $coord->getData()->Seguimiento_simpleResult;
        if($datos->guias_vinculadas){
            $guias = 'No tiene';
        }else{
            $guias = $datos->guias_vinculadas;
        }
        $val = '
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Codigo de remision</th>
                        <th>Estado</th>
                        <th>Novedad</th>
                        <th>Referencia</th>
                        <th>Nombre de origen</th>
                        <th>Nombre de destino</th>
                        <th>Producto</th>
                        <th>Guias vinculadas</th>
                        <th>Dias promesa servicio</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.$datos->codigo_remision.'</td>
                        <td>'.$datos->estado->descripcion.'</td>
                        <td>'.$datos->novedad->descripcion.'</td>
                        <td>'.$datos->referencia.'</td>
                        <td>'.$datos->nombre_origen.'</td>
                        <td>'.$datos->nombre_destino.'</td>
                        <td>'.$datos->producto.'</td>
                        <td>'.$guias.'</td>
                        <td>'.$datos->dias_promesa_servicio.'</td>
                    </tr>
                </tbody>
            </table>
        ';
    }else{
        echo $coord->getData();
    }
?>
