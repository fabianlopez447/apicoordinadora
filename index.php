<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <meta charset="UTF-8">
    <title>API coordinadora</title>
</head>
<body>
    <div class="container-fluid">  
        <br>     
        <h2 class="text-center">Seguimiento de pequetes de paquetes Coordinadora</h2>
        <br>   
        <div class="row contenido">  
        <div class="col-md-12">
            <ul class="nav nav-tabs nav-fill">
              <li class="nav-item">
                <a class="nav-link active" href="#">Seguimiento Simple</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Seguimiento Detallado</a>
              </li>
            </ul>
        </div> 
            <div class="col-md-6 text-center">
                <br>
                <div class="card">
                  <div class="card-body">
                    <h6 class="card-title">Por favor, ingrese en este campo el numero o numeros de sus guias Nacionales o Internacionales, Separados por Enter</h6>
                    <p class="card-text"><textarea class="form-control" name="" id="" cols="60" rows="5" placeholder=""></textarea></p>
                    <a href="#" class="btn btn-primary">Rastrear Guia(s)</a>
                  </div>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <br>
                <div class="card">
                  <div class="card-body">
                    <h6 class="card-title">Por favor, ingrese en este campo el numero o numeros de sus guias Nacionales o Internacionales, Separados por Enter</h6>
                    <p class="card-text"><textarea class="form-control" name="" id="" cols="60" rows="5" placeholder=""></textarea></p>
                    <a href="#" class="btn btn-primary">Rastrear Guia(s)</a>
                  </div>
                </div>
            </div> 
        </div> 
    
    </div>
    
    
    <script src="js/bootstrap.min.js"></script>
</body>
</html>